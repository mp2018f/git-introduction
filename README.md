## How to create your ionic project and link with Bitbucket repo

You�ll start by editing this README file to learn how to edit a file in Bitbucket.

1. **Bitbucket:** create a repository **without README.md**, private
2. **Bitbucket:** Copy repository URL
3. **COMPUTER:** ionic start <project> <template>
4. **COMPUTER:** cd <project>
5. **COMPUTER:** git remote add origin <URL>
6. **COMPUTER:** git push -u origin master
7. **COMPUTER:** (work on your project)
8. **COMPUTER:** git add .
9. **COMPUTER:** git commit -m "my changes"
10. **COMPUTER:** git push
11. REPEAT 7 ~ 10

---

## How to download Professor's lab code 

After cloning the repository, you will have to run "npm install" to repopulate missing files that were excluded for push to save the upload size.

1. **Class Homepage:** Click on the repository link
2. **Bitbucket:** Copy URL
3. **COMPUTER:** git clone <URL>
4. **COMPUTER:** cd <project>
5. **COMPUTER:** npm install
6. **COMPUTER:** ionic serve

---

## How to submit your home work 

* Please name your project in Bitbucket to "HW#_<student_id>" (For example, HW2_6302321)

1. Follow the instruction above to create your local project files and sync with Bitbucket with the correct project name
2. Make sure your Bitbucket repository is **Private**
3. Make sure your Bitbucket repository is shared with **mp2018f@gmail.com** with admin access right
4. Make sure you never share your repository with other students
5. By the deadline, email to **mp2018f@gmail.com** with subject "HW#_<student_id>" and content the Bitbucket repository URL (Skip)
6. Make sure you commit all your changes and push to Bitbucket **BEFORE THE DEADLINE**. Any commits/pushes after the deadline will be ignored.

